<h1 class="code-line" data-line-start=0 data-line-end=1 ><a id="Deworming_Project_0"></a>Deworming Project</h1>
<p class="has-line-data" data-line-start="1" data-line-end="2"><a href="https://ird.global/"><img src="https://ird.global/wp-content/uploads/2021/07/ird-logo.png" alt="N|Solid"></a></p>
<h2 class="code-line" data-line-start=3 data-line-end=4 ><a id="Getting_Started_3"></a>Getting Started</h2>
<p class="has-line-data" data-line-start="4" data-line-end="6">Download and Install the latest version of CSPRO from the following link<br>
<a href="https://www.csprousers.org/downloads/">https://www.csprousers.org/downloads/</a></p>
<p class="has-line-data" data-line-start="7" data-line-end="9">On start CSPRO Screen open an existing project<br>
<img src="https://i.ibb.co/9wwZVvg/Screenshot-2022-07-22-at-6-45-29-PM.png" alt="N|Solid"></p>
<p class="has-line-data" data-line-start="10" data-line-end="11">There are three Data entry Application open the following the forms</p>
<ul>
<li class="has-line-data" data-line-start="12" data-line-end="13">deworm/Circle Level Form.ent</li>
<li class="has-line-data" data-line-start="13" data-line-end="14">deworm/District Level Form.ent</li>
<li class="has-line-data" data-line-start="14" data-line-end="16">deworm/province Level Form.ent</li>
</ul>
<p class="has-line-data" data-line-start="16" data-line-end="17"><img src="https://i.ibb.co/bKtcHhL/Screenshot-2022-07-22-at-6-44-24-PM.png" alt="N|Solid"></p>
<p class="has-line-data" data-line-start="18" data-line-end="19">Run the selected application</p>
<p class="has-line-data" data-line-start="20" data-line-end="21"><img src="https://i.ibb.co/MPH9d4R/Screenshot-2022-07-22-at-7-08-29-PM.png" alt="N|Solid"></p>
<h2 class="code-line" data-line-start=21 data-line-end=22 ><a id="Connect_CSPRO_with_Dropbox_21"></a>Connect CSPRO with Dropbox</h2>
<ol>
<li class="has-line-data" data-line-start="23" data-line-end="24">
<p class="has-line-data" data-line-start="23" data-line-end="24">Create a dropbox account with your official email.</p>
</li>
<li class="has-line-data" data-line-start="24" data-line-end="25">
<p class="has-line-data" data-line-start="24" data-line-end="25">Goto shared and you will see the CSPRO folder. Add that folder to your dropbox account.</p>
</li>
<li class="has-line-data" data-line-start="25" data-line-end="26">
<p class="has-line-data" data-line-start="25" data-line-end="26">Download the CSEntry application from Playstore</p>
</li>
<li class="has-line-data" data-line-start="26" data-line-end="29">
<p class="has-line-data" data-line-start="26" data-line-end="28">Open CSEntry Application and goto Add Application button<br>
[<img src="https://lh3.googleusercontent.com/noDx8kC1-OvT0iTYHKa2NS8x7XrO3xEKssNNevUFp1B37G4tdRD-pb70SosogV_qKoMmbfY4rgF6qaRRqRdXtFyZNsC-n9KLAmRYygM4ltH7lwpw7C7ZvKV5ktUEiY-kM52yITQjrjch47Bh9pDW8w" alt="N|Solid">]</p>
</li>
<li class="has-line-data" data-line-start="29" data-line-end="32">
<p class="has-line-data" data-line-start="29" data-line-end="31">Select Dropbox and sign in with the same account you have created<br>
[<img src="https://lh3.googleusercontent.com/4vKQPd_PZpvo0o7r8axxUpuy3EZvue2m4HTWVCdMt5nNq725t_6GETmJA2NGTcwIVzh4RzCAwY7HSNWtsU-PbC49qjQ3axKnTmV_yXXKd-N_lUEOs7lg28zeR-gYYP6YBV1fNccKYf7zV28V1o-WkQ" alt="N|Solid">]</p>
</li>
<li class="has-line-data" data-line-start="32" data-line-end="34">
<p class="has-line-data" data-line-start="32" data-line-end="34">Install/Update the “Deworm Application” from the list<br>
[<img src="https://lh6.googleusercontent.com/QkVKz09oCDbwBUOgMQuFBX9cRUEzNwjHPsUsz146Rr8ZQGcR5qpjzNtAwM9ZBMEc7WKF-yA3IJmCmFobONhLTOneJqCDwaql6M1Ks00EnLSy1yoR5TT_tq6M5fWEfHeMyld9jfpSSemxzGq6XeDHnQ" alt="N|Solid">]</p>
</li>
<li class="has-line-data" data-line-start="34" data-line-end="36">
<p class="has-line-data" data-line-start="34" data-line-end="36">You will see three application forms for data entry.<br>
[<img src="https://lh3.googleusercontent.com/WgbX5zn4Q2f6fBWUuRXuRjCMXI4ZXKh98UGdPhWvxyT8P4oip6kXBjRwwYYcfXlv-WC_Y9Ktw-SSMFGVhoP_dMoxL9Bmz77O-H4mki_5JGENeSQi4Hdz7FT_8_Igf3F6bPZzdlxXAPHAh-3BIgqpnQ" alt="N|Solid">]</p>
</li>
</ol>
